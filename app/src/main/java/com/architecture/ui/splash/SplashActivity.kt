package com.architecture.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.architecture.R
import com.architecture.base.BaseActivity
import com.architecture.ui.home.HomeActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed(Runnable {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }, 1500)
    }
}
