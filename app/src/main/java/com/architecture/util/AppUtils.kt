package com.architecture.util

import android.app.Activity
import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.multidex.BuildConfig
import com.architecture.R
import com.blankj.utilcode.util.SnackbarUtils
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject


open class AppUtils {


    @Inject
    lateinit var preferenceUtils: PreferenceUtils

    companion object {
        var INSTANCE: AppUtils? = null

        fun setInstance() {
            if (INSTANCE == null) {
                INSTANCE = AppUtils()
            }
        }
    }

    fun logMe(tag: String, message: String?) {
        if (!TextUtils.isEmpty(tag) && !TextUtils.isEmpty(message) && BuildConfig.DEBUG) {
            Log.e(tag, message?:"")
        }
    }

    fun hideKeyboard(mAcivity: Activity?) {
        val view = mAcivity?.currentFocus
        if (view != null) {
            val imm = mAcivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showView(view: View, duration: Long) {
        //Alpha 1 indicates completely visible
        view.animate().alpha(1.0f).duration = 400

        //        view.visibility = View.VISIBLE
        //            .setListener(object : AnimatorListenerAdapter() {
        //                override fun onAnimationEnd(animation: Animator) {
        //                    view.setVisibility(View.GONE)
        //                }
        //            })
    }

    fun hideView(view: View, duration: Long) {
        //alpha 0 indicates completely transaparent
        //        view.visibility = View.GONE
        view.animate().alpha(0f).duration = 400
        //            .setListener(object : AnimatorListenerAdapter() {
        //                override fun onAnimationEnd(animation: Animator) {
        //                    view.setVisibility(View.GONE)
        //                }
        //            })
    }

    fun showFadeView(view: View, duration: Long) {
        //        view.visibility = View.VISIBLE

        view.animate().alpha(1.0f).duration = duration
        //            .setListener(object : AnimatorListenerAdapter() {
        //                override fun onAnimationEnd(animation: Animator) {
        //                    view.setVisibility(View.GONE)
        //                }
        //            })
    }

    fun hideFadeView(view: View, duration: Long) {
        //        view.visibility = View.GONE

        view.animate().alpha(0.2f).duration = duration
        //            .setListener(object : AnimatorListenerAdapter() {
        //                override fun onAnimationEnd(animation: Animator) {
        //                    view.setVisibility(View.GONE)
        //                }
        //            })
    }

    fun showSnackBar(msg: String, view: View) {
        SnackbarUtils.with(view).setMessage(msg).setBgColor(
            ContextCompat.getColor(view.context, R.color.colorPrimary)
        ).show()
    }


    fun preventTwoClick(view: View) {
        /**
         * To block click on view for some milliseconds
         * To avoid repetative clicks on view, so that desired action is not performed twice
         *
         */
        view.isEnabled = false
        view.postDelayed({ view.isEnabled = true }, 500)
    }


}


