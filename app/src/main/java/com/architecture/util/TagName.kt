package com.architecture.util

class TagName {
    companion object {

        val API_TAG = "API_TAG"
        val EXCEPTION_TAG = "EXCEPTION_TAG"

    }
}