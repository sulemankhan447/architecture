package com.architecture.util

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import android.widget.ImageView
import com.architecture.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


final class ImageUtils {

    companion object {
        var INSTANCE: ImageUtils? = null

        fun setImageInstance() {
            if (INSTANCE == null) {
                INSTANCE = ImageUtils()
            }
        }
    }
//
//    fun loadBlogsRemoteImage(imageView: ImageView?, imageUrl: String?, letterName: String?) {
//        imageView?.run {
//            Glide.with(context).load(imageUrl)
//                .override(width, height)
//                .placeholder(R.drawable.ic_fanaboard_loader_optimized)
//                .error(R.drawable.ic_fanaboard_loader_optimized)
//                .downsample(DownsampleStrategy.CENTER_INSIDE)
//                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                .into(this)
//
//        }
//    }
//
//
//    fun loadRemoteImageForProfile(imageView: ImageView?, imageUrl: String?, letterName: String?) {
//
//        if (!StringUtils.isEmpty(imageUrl)) {
//            imageView?.run {
//                Glide.with(context)
//                    .load(imageUrl)
//                    .override(width, height)
//                    .placeholder(R.drawable.ic_fanaboard_loader_optimized)
//                    .error(getTextDrawableForProfile(imageView.context, letterName))
//                    .downsample(DownsampleStrategy.CENTER_INSIDE)
//                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                    .into(this)
//            }
//        } else {
//            showTextDrawableForProfile(imageView, letterName)
//
//
//        }
//    }
//
//    fun loadTourScheduleImage(imageView: ImageView?, imageUrl: String?) {
//        //To Show Tour Schedule Image From Api
//        imageView?.run {
//            Glide.with(context)
//                .load(imageUrl)
//                .override(width, height)
//                .placeholder(R.drawable.ic_fanaboard_loader_optimized)
//                .error(R.drawable.ic_match_placeholder)     //If Image Fail Show match Placeholder
//                .downsample(DownsampleStrategy.CENTER_INSIDE).diskCacheStrategy(
//                    DiskCacheStrategy.AUTOMATIC).into(this)
//
//        }
//    }
//
//

    fun loadRemoteImage(imageView: ImageView?, imageUrl: String?) {
        imageView?.context?.let {
            Glide.with(it)
                .load(imageUrl)
                .into(imageView)
        }
    }


    //    fun loadRemoteImage(imageView: ImageView?, imageUrl: String?, letterName: String?,
//        fromGroup: Boolean = false, memberNo: Int = -1) {
//        if (!StringUtils.isEmpty(imageUrl)) {
//            imageView?.run {
//                Glide.with(context)
//                    .load(imageUrl)
//                    .override(width, height).placeholder(
//                    R.drawable.ic_fanaboard_loader_optimized)
//                    .error(getTextDrawable(imageView.context, letterName))
//                    .downsample(DownsampleStrategy.CENTER_INSIDE)
//                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                    .into(this)
//            }
//        } else {
//            if (fromGroup) {
//                showTextDrawable(imageView, letterName, fromGroup, memberNo)
//
//            } else {
//                showTextDrawable(imageView, letterName)
//
//            }
//        }
//    }
//
//
//    private fun showTextDrawableForProfile(imageView: ImageView?, letterName: String?) {
//        imageView?.let {
//            val imgDrawable = getTextDrawableForProfile(imageView.context, letterName)
//            it.setImageBitmap(com.blankj.utilcode.util.ImageUtils.drawable2Bitmap(imgDrawable))
//        }
//    }
//
//
//    private fun showTextDrawable(imageView: ImageView?, letterName: String?,
//        fromGroup: Boolean = false, memberNo: Int = -1) {
//        imageView?.let {
//            val imgDrawable = getTextDrawable(imageView.context, letterName, fromGroup, memberNo)
//            it.setImageBitmap(com.blankj.utilcode.util.ImageUtils.drawable2Bitmap(imgDrawable))
//        }
//    }
//
//    private fun showTextDrawableForGroup(imageView: ImageView?, letterName: String?) {
//        imageView?.let {
//            val imgDrawable = getTextDrawableForGroup(imageView.context, letterName)
//            it.setImageBitmap(com.blankj.utilcode.util.ImageUtils.drawable2Bitmap(imgDrawable))
//        }
//    }
//
//
//    private fun getTextDrawableForProfile(context: Context, letterName: String?): TextDrawable? {
//
//        val mDefaultName = AppUtils.INSTANCE?.getInitials(context.getString(R.string.app_name))
//        val letter = if (TextUtils.isEmpty(
//                letterName)) mDefaultName else AppUtils.INSTANCE?.getInitials(letterName)
//
//        return TextDrawable.builder().beginConfig().textColor(
//            ContextCompat.getColor(context, R.color.black)).useFont(Typeface.DEFAULT).fontSize(
//            20).toUpperCase().width(100).height(100).endConfig().buildRound(letter,
//            ContextCompat.getColor(context, R.color.blue_variant))
//    }
//
//
    fun saveImage(myBitmap: Bitmap, context: Context): String {
        /**
         * Returns absolute Path of Image
         */
        val pictureFile = createFile(context)
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val byteArray = bytes.toByteArray()
        try {
            val fos = FileOutputStream(pictureFile)
            fos.write(byteArray)
            fos.close()
        } catch (error: Exception) {
            AppUtils.INSTANCE?.logMe(
                TagName.API_TAG, "File" + pictureFile.name + "not saved: "
                        + error.message
            )
        }
        return pictureFile.absolutePath
    }

    fun createFile(context: Context): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            //            mCurrentPhotoPath = absolutePath
        }
    }

    //
//
//
//    private fun getTextDrawable(context: Context, letterName: String?, fromGroup: Boolean = false,
//        memberNo: Int = -1): TextDrawable? {
//
//        val mDefaultName = AppUtils.INSTANCE?.getInitials(context.getString(R.string.app_name))
//        val letter = if (TextUtils.isEmpty(
//                letterName)) mDefaultName else AppUtils.INSTANCE?.getInitials(letterName)
//
//        //        AppUtils.INSTANCE?.logMe("TEXT DRAWABLE", "LETTER : $letter ")
//
//        if (fromGroup) {
//            if (memberNo == 0) {
//                return TextDrawable.builder().beginConfig().textColor(
//                    ContextCompat.getColor(context, R.color.black)).useFont(
//                    Typeface.DEFAULT).fontSize(20).toUpperCase().width(60).height(
//                    60).endConfig().buildRound(letter,
//                    ContextCompat.getColor(context, R.color.member_1_color))
//            } else if (memberNo == 1) {
//                return TextDrawable.builder().beginConfig().textColor(
//                    ContextCompat.getColor(context, R.color.black)).useFont(
//                    Typeface.DEFAULT).fontSize(20).toUpperCase().width(60).height(
//                    60).endConfig().buildRound(letter,
//                    ContextCompat.getColor(context, R.color.member_2_color))
//            } else if (memberNo == 2) {
//                return TextDrawable.builder().beginConfig().textColor(
//                    ContextCompat.getColor(context, R.color.black)).useFont(
//                    Typeface.DEFAULT).fontSize(20).toUpperCase().width(60).height(
//                    60).endConfig().buildRound(letter,
//                    ContextCompat.getColor(context, R.color.member_3_color))
//            } else if (memberNo == 3) {
//                return TextDrawable.builder().beginConfig().textColor(
//                    ContextCompat.getColor(context, R.color.black)).useFont(
//                    Typeface.DEFAULT).fontSize(20).toUpperCase().width(60).height(
//                    60).endConfig().buildRound(letter,
//                    ContextCompat.getColor(context, R.color.member_4_color))
//            }
//        }
//
//
//
//        return TextDrawable.builder().beginConfig().textColor(
//            ContextCompat.getColor(context, R.color.black)).useFont(Typeface.DEFAULT).fontSize(
//            20).toUpperCase().width(60).height(60).endConfig().buildRound(letter,
//            ContextCompat.getColor(context, R.color.blue_variant))
//    }
//
//    private fun getTextDrawableForGroup(context: Context, letterName: String?): TextDrawable? {
//
//        val mDefaultName = AppUtils.INSTANCE?.getInitials(context.getString(R.string.app_name))
//        val letter = if (TextUtils.isEmpty(
//                letterName)) mDefaultName else AppUtils.INSTANCE?.getInitials(letterName)
//
//        //        AppUtils.INSTANCE?.logMe("TEXT DRAWABLE", "LETTER : $letter ")
//
//        return TextDrawable.builder().beginConfig().textColor(
//            ContextCompat.getColor(context, R.color.black)).useFont(Typeface.DEFAULT).fontSize(
//            20).toUpperCase().width(60).height(60).endConfig().buildRound(letter,
//            ContextCompat.getColor(context, R.color.blue_variant))
//    }
//
    fun loadLocalImage(imageView: ImageView?, image: File?) {
        try {
            imageView?.run {
                Glide.with(context)
                    .load(image)
                    .override(width, height)
//                    .placeholder(R.drawable.ic_fanaboard_loader_optimized)
                    .downsample(DownsampleStrategy.CENTER_INSIDE)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(this)
            }
        } catch (e: Exception) {
        }
//
//        Glide.with(mContext)
//            .load(new File (pictureUri.getPath())) // Uri of the picture
//            .transform(new CircleTransform (..))
//        .into(profileAvatar);


    }

    //
    fun loadLocalGIFImage(imageView: ImageView, image: Int) {
        try {
            imageView.run {
                Glide.with(context)
                    .asGif()
                    .load(image)
                    .override(width, height)
                    .placeholder(R.drawable.loader)
                    .downsample(DownsampleStrategy.CENTER_INSIDE)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(this)
            }
        } catch (e: Exception) {
        }
    }
//
//    fun loadRemoteImageForCountry(imageView: ImageView?, imageUrl: String?, letterName: String?) {
//        /**
//         * Used to show Image for country in PA Match/Tournament For Players & Teams
//         */
//        if (!StringUtils.isEmpty(imageUrl)) {
//            imageView?.run {
//                Glide.with(context)
//                    .load(imageUrl)
//                    .override(width, height)
//                    .placeholder(R.drawable.ic_fanaboard_loader_optimized)
//                    .error(getTextDrawable(imageView.context, letterName))
//                    .downsample(DownsampleStrategy.CENTER_INSIDE)
//                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                    .into(this)
//            }
//        } else {
//            imageView?.visibility = View.INVISIBLE
//        }
//    }
//
//    fun loadRemoteImageForMatch(imageView:ImageView,imageUrl: String?,name:String?){
//
//        if(imageUrl?.isNotEmpty()?:false)
//        {
//            ImageUtils.INSTANCE?.loadRemoteImage(imageView,imageUrl,name)
//        }
//        else{
//            imageView.setImageResource(R.drawable.ic_match_placeholder)
//        }
//
//    }
//
//    fun loadRemoteImageForTournament(imageView:ImageView,imageUrl: String?,name:String?){
//
//        if(imageUrl?.isNotEmpty()?:false)
//        {
//            ImageUtils.INSTANCE?.loadRemoteImage(imageView,imageUrl,name)
//        }
//        else{
//            imageView.setImageResource(R.drawable.ic_tournament_placeholder)
//        }
//
//    }

}