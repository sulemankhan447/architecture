package com.architecture.base

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.architecture.Constants
import com.architecture.R
import com.architecture.di.application.ApplicationComponent
import com.architecture.util.AppUtils
import com.architecture.util.ImageUtils
import com.architecture.util.PreferenceUtils
import com.google.android.material.snackbar.Snackbar
import com.washiteria.di.screen.ScreenModule
import javax.inject.Inject

open class BaseActivity : AppCompatActivity() {
    private var snackbar: Snackbar? = null

    @Inject
    lateinit var preferenceUtils: PreferenceUtils

    val screenComponent by lazy {
        getApplicationComponent().plus(ScreenModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenComponent.inject(this)
    }


    private fun getApplicationComponent(): ApplicationComponent {
        return (application as BaseApplication).component
    }

//    fun showSnackBar(msg: String, view: View) {
//        snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
//        snackbar?.show()
//    }

    fun isTutorialShown(type: String): Boolean {
        return preferenceUtils.getValue(type, false)
    }

    public fun toggleView(parent: View, loader: View, imageView: ImageView, isShow: Boolean) {
        if (isShow) {
            AppUtils.INSTANCE?.hideView(parent, Constants.VIEW_ANIMATE_DURATION)
            AppUtils.INSTANCE?.showView(loader, Constants.VIEW_ANIMATE_DURATION)
            ImageUtils.INSTANCE?.loadLocalGIFImage(imageView, R.drawable.loader)
        } else {
            AppUtils.INSTANCE?.hideView(loader, Constants.VIEW_ANIMATE_DURATION)
            AppUtils.INSTANCE?.showView(parent, Constants.VIEW_ANIMATE_DURATION)
        }
    }

    public fun toggleFadeView(
        parent: View,
        loader: View,
        imageView: ImageView,
        showLoader: Boolean
    ) {

        if (showLoader) {
            AppUtils.INSTANCE?.hideFadeView(parent, Constants.VIEW_ANIMATE_DURATION)
            AppUtils.INSTANCE?.showFadeView(loader, Constants.VIEW_ANIMATE_DURATION)
            ImageUtils.INSTANCE?.loadLocalGIFImage(imageView, R.drawable.loader)
        } else {
            AppUtils.INSTANCE?.hideView(loader, Constants.VIEW_ANIMATE_DURATION)
            AppUtils.INSTANCE?.showView(parent, Constants.VIEW_ANIMATE_DURATION)
        }
    }

}