package com.architecture.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.architecture.R
import com.architecture.di.application.ApplicationComponent
import com.architecture.util.PreferenceUtils
import com.washiteria.di.screen.FragmentModule
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
open class BaseFragment : Fragment() {

    @Inject
    lateinit var preferenceUtils: PreferenceUtils

    public val screenComponent by lazy {
        getApplicationComponent().plusFragment(FragmentModule(this))
    }

    private fun getApplicationComponent(): ApplicationComponent {
        return (activity?.application as BaseApplication).component
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        screenComponent.inject(this)
        return super.onCreateView(inflater, container, savedInstanceState)
    }



    public fun toggleView(parent: View, loader: View, imageView: ImageView, isHide: Boolean) {
        if (isHide) {
//            AppUtils.INSTANCE?.hideView(parent, VIEW_ANIMATE_DURATION)
//            AppUtils.INSTANCE?.showView(loader, VIEW_ANIMATE_DURATION)
//            ImageUtils.INSTANCE?.loadLocalGIFImage(
//                imageView,
//                R.drawable.ic_fanaboard_loader_optimized
//            )
        } else {
//            AppUtils.INSTANCE?.hideView(loader, VIEW_ANIMATE_DURATION)
//            AppUtils.INSTANCE?.showView(parent, VIEW_ANIMATE_DURATION)
        }
    }

    public fun toggleFadeView(
        parent: View,
        loader: View,
        imageView: ImageView,
        showLoader: Boolean
    ) {

        if (showLoader) {
//            AppUtils.INSTANCE?.hideFadeView(parent, VIEW_ANIMATE_DURATION)
//            AppUtils.INSTANCE?.showFadeView(loader, VIEW_ANIMATE_DURATION)
//            ImageUtils.INSTANCE?.loadLocalGIFImage(
//                imageView,
//                R.drawable.ic_fanaboard_loader_optimized
//            )
        } else {
//            AppUtils.INSTANCE?.hideView(loader, VIEW_ANIMATE_DURATION)
//            AppUtils.INSTANCE?.showView(parent, VIEW_ANIMATE_DURATION)
        }
    }


}