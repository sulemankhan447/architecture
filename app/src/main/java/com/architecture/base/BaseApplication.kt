package com.architecture.base

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import com.architecture.di.application.ApplicationComponent
import com.architecture.di.application.ApplicationModule
import com.architecture.di.application.DaggerApplicationComponent
import com.architecture.util.AppUtils
import com.architecture.util.ImageUtils
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class BaseApplication : Application() {
    /**
     * Fonts missing
     * Styles of fonts missing
     * Base view holder missing
     * Add properties of shapeable image view
     */

    lateinit var component: ApplicationComponent

    companion object {
        private var instance: BaseApplication? = null
        var isActive: Boolean = false
        var isConnected: Boolean = false

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        initNetworkListener()
        initSingleton()
        inject()
        // This flag should be set to true to enable VectorDrawable support for API < 21
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }


    private fun initSingleton() {
        ImageUtils.setImageInstance()
        AppUtils.setInstance()
    }

    fun inject() {
        component = DaggerApplicationComponent.builder().applicationModule(
            ApplicationModule(this)
        ).build()
        component.inject(this)
        instance = this
    }

    @SuppressLint("CheckResult")
    fun initNetworkListener() {
        ReactiveNetwork
            .observeNetworkConnectivity(applicationContext)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                isConnected = it.available()
                AppUtils.INSTANCE?.logMe("Network Connectivity Activity : ", isConnected.toString())
            }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}