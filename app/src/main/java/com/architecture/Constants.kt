package com.architecture

class Constants {
    companion object {
        const val VIEW_ANIMATE_DURATION = 400L
        const val VIEW_ANIMATE_DELAY = 400L
        const val SUCCESS = "SUCCESS"
        const val RETROFIT_BASE = "RETROFIT_BASE"
        const val USER_TOKEN = "USER_TOKEN"
    }


}