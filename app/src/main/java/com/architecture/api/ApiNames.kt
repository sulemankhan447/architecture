package com.architecture.api

class ApiNames {
    companion object {
        const val BASE_URL = "www.google.com"
        const val LOGIN = "login"
    }
}