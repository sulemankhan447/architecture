package com.architecture.api

import com.architecture.ui.login.model.LoginResponseModel
import retrofit2.Call
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface NetworkEndpoint {


    @FormUrlEncoded
    @POST(ApiNames.LOGIN)
    fun callLoginApi(@FieldMap login: HashMap<String, String>): Call<LoginResponseModel>


}