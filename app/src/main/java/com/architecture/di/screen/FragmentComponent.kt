package com.architecture.di.screen

import com.architecture.base.BaseFragment
import com.architecture.di.scope.PerScreen
import com.washiteria.di.screen.FragmentModule
import dagger.Subcomponent


/*  Magic Box of Dagger2 which does the Poke */

/* Whenever a New Activity is Created,It Inject Function Should be Declared Here */

@PerScreen
@Subcomponent(modules = [FragmentModule::class])
interface FragmentComponent {
    /* interface MagicBox */
    /* Inject Indicates Telling Magic Box, In Which Activity It Should Perform its Magic,thus we have parameter of that activity in inject */

    /*  Injecting in Activity Means Instantiating Variables of That Activity Using Dagger 2 */

    fun inject(baseFragment: BaseFragment)


}
