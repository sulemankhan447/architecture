package com.washiteria.di.screen

import com.architecture.base.BaseFragment
import com.architecture.di.scope.PerScreen
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(private val activity: BaseFragment) {

    @PerScreen
    @Provides
    fun providesFragment(): BaseFragment {
        return activity
    }

}
