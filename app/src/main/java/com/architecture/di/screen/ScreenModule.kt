package com.washiteria.di.screen

import com.architecture.base.BaseActivity
import com.architecture.di.scope.PerScreen
import dagger.Module
import dagger.Provides

@Module
class ScreenModule(private val activity: BaseActivity) {

    @PerScreen
    @Provides
    fun providesActivity(): BaseActivity {
        return activity
    }


}

