package com.washiteria.di.screen

import com.architecture.base.BaseActivity
import com.architecture.di.scope.PerScreen
import com.architecture.ui.splash.SplashActivity
import dagger.Subcomponent


/*  Magic Box of Dagger2 which does the Poke */

/* Whenever a New Activity is Created,It Inject Function Should be Declared Here */

@PerScreen
@Subcomponent(modules = [ScreenModule::class])
interface ScreenComponent {
    /* interface MagicBox */
    /* Inject Indicates Telling Magic Box, In Which Activity It Should Perform its Magic,thus we have parameter of that activity in inject */

    /*  Injecting in Activity Means Instantiating Variables of That Activity Using Dagger 2 */

    //Knsi knsi screen main inject krna hain dependencies woh screen yaha pr neeche defined hain aur kya inject krna hain woh Screen Module main defined Hain

    fun inject(baseActivity: BaseActivity)

    fun inject(splashActivity: SplashActivity)

}


