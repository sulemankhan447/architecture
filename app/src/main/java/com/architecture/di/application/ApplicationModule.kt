package com.architecture.di.application


import com.architecture.Constants.Companion.RETROFIT_BASE
import com.architecture.Constants.Companion.USER_TOKEN
import com.architecture.api.ApiNames
import com.architecture.base.BaseApplication
import com.architecture.util.PreferenceUtils
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton


@Module
class ApplicationModule(private val application: BaseApplication) {

    private val cacheSize = (5 * 1024 * 1024).toLong()
    private val cache = Cache(application.cacheDir, cacheSize)

    @Provides
    @Singleton
    fun provideApplication(): BaseApplication {
        return application
    }

    @Provides
    @Singleton
    fun providePreferenceUtils(): PreferenceUtils {
        return PreferenceUtils(application)
    }



    @Provides
    @Singleton
    @Named(RETROFIT_BASE)
    fun provideRetrofit(): Retrofit {

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)

        val okHttpClient = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .callTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .cache(cache)
            .addInterceptor(ChuckerInterceptor(BaseApplication.applicationContext()))
            .addInterceptor(object : Interceptor {
                @Throws(IOException::class)
                override fun intercept(
                    chain: Interceptor.Chain
                ): Response {
                    val original = chain.request()

                    val requestBuilder: Request.Builder
//                    val mUrl = original.url.toString()
                    val mUrl = original.url().toString()
                    val token = "Bearer " + providePreferenceUtils().getValue(USER_TOKEN)
//                        // Request customization: add request headers
                    requestBuilder = original.newBuilder()
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                    val request = requestBuilder.build()
                    return chain.proceed(request)
                }
            })
            .build()
        val gson = GsonBuilder().setLenient().create()

        return Retrofit.Builder()
            .baseUrl(ApiNames.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    }


}
