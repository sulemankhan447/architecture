package com.architecture.di.application

import com.architecture.base.BaseApplication
import com.architecture.di.screen.FragmentComponent
import com.washiteria.di.screen.FragmentModule
import com.washiteria.di.screen.ScreenComponent
import com.washiteria.di.screen.ScreenModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ApplicationModule::class, EndpointModule::class])
interface ApplicationComponent {

    fun inject(application: BaseApplication)
    fun plus(screenModule: ScreenModule): ScreenComponent
    fun plusFragment(fragmentModule: FragmentModule): FragmentComponent
}
