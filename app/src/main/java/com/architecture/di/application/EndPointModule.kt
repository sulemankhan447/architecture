package com.architecture.di.application


import com.architecture.Constants
import com.architecture.api.NetworkEndpoint
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton


@Module
class EndpointModule {

    @Provides
    @Singleton
    fun provideNetworkEndpoint(@Named(Constants.RETROFIT_BASE) retrofit: Retrofit): NetworkEndpoint {
        return retrofit.create(NetworkEndpoint::class.java)
    }
}
